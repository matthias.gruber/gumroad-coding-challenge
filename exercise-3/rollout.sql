Select SUM(price) as turnover, seller_id
FROM Product
         JOIN (Select SUM(price) as price, product_id
               FROM (

                     # Select refunds with negative price in the desired range
                     Select 0 - price as price, product_id
                     from Refund
                              JOIN Purchase P on Refund.purchase_id = P.purchase_id
                     WHERE refund_date BETWEEN '2021-04-5' AND '2021-04-12'


                     UNION ALL # Union them with purchases in the desired range
                     Select price, product_id
                     from Purchase
                     WHERE purchase_date BETWEEN '2021-04-5' AND '2021-04-12') as product_prices

                # Group them to by product to sum up their prices/refunds
               GROUP BY product_id) as product_turnover ON Product.product_id = product_turnover.product_id
# Join them with the product (see on top) to get the seller_id
# Group by seller_id to get the turnover of all products for each selelr
GROUP BY seller_id;
