enum BUTTON_OPTION {
  SINGLE_PRODUCT = 'data-gumroad-single-product',
  PRELOAD = 'data-gumroad-hover-preload',
  PRODUCT_URL = 'href'
}

export class GumroadButton {
  public constructor(private element: Element) {
  }

  private getAttributeValue(option: BUTTON_OPTION): string | undefined {
    return this.element.attributes.getNamedItem(option)?.value
  }

  private isOptionActivated(option: BUTTON_OPTION): boolean {
    return this.getAttributeValue(option) === 'true';
  }

  public getUrl(): string {
    let url = this.getAttributeValue(BUTTON_OPTION.PRODUCT_URL)!;
    if (this.isOptionActivated(BUTTON_OPTION.SINGLE_PRODUCT)) {
      url += 'single_product_mode=true';
    }
    return url;
  }

  public get shouldPreload(): boolean {
    return this.isOptionActivated(BUTTON_OPTION.PRELOAD);
  }
}
