export class LoadingIndicator {
  public loadingIndicator: HTMLDivElement;

  public constructor() {
    this.loadingIndicator = document.createElement("div");
    this.loadingIndicator.className = "gumroad-loading-indicator";
    this.loadingIndicator.appendChild(document.createElement("i"));
    document.body.append(this.loadingIndicator);
  }

  public show() {
    this.loadingIndicator.setAttribute('style', 'display: block');
  }

  public hide() {
    this.loadingIndicator.setAttribute('style', 'display: none');
  }
}
