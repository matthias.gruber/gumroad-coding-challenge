export function createStyleTag(css: string): HTMLStyleElement {
  const styleTag = document.createElement('style');

  styleTag.type = 'text/css';
  styleTag.appendChild(document.createTextNode(css));
  return styleTag;
}

export function addStyleSheet(domain: string) {
  const css = getStyles(domain);
  const styleTag = createStyleTag(css);
  document.head.appendChild(styleTag);
}

function getStyles(domain: string): string {
  const gumroadTheme = 'a.gumroad-button { background-color: white !important; background-image: url("GUMROAD_ORIGIN/button/button_bar.jpg") !important; background-repeat: repeat-x !important; border-radius: 4px !important; box-shadow: rgba(0, 0, 0, 0.4) 0 0 2px !important; color: #999 !important; display: inline-block !important; font-family: -apple-system, ".SFNSDisplay-Regular", "Helvetica Neue", Helvetica, Arial, sans-serif !important; font-size: 16px !important; font-style: normal !important; font-weight: 500 !important; line-height: 50px !important; padding: 0 15px !important; text-shadow: none !important; text-decoration: none !important; } .gumroad-button-logo { background-image: url("GUMROAD_ORIGIN/button/button_logo.png") !important; background-size: cover !important; height: 17px !important; width: 16px !important; display: inline-block !important; margin-bottom: -3px !important; margin-right: 15px !important; } .gumroad-loading-indicator { background: white; border-radius: 50%; box-shadow: 0 1px 2px rgba(0, 0, 0, 0.1); box-sizing: border-box; display: none; height: 60px; left: 50% !important; margin-left: -30px !important; margin-top: -30px !important; padding: 10px; position: fixed; top: 50% !important; width: 60px; z-index: 99997; } .gumroad-loading-indicator i { background: url("GUMROAD_ORIGIN/js/loading-rainbow.svg"); height: 40px; width: 40px; display: inline-block; background-size: contain; animation: gumroad-spin 1.5s infinite linear; } .gumroad-scroll-container { -webkit-overflow-scrolling: touch; overflow-y: auto; position: fixed !important; z-index: 99998 !important; top: 0 !important; right: 0 !important; -ms-overflow-style: none; scrollbar-width: none; text-align: start; } .gumroad-scroll-container::-webkit-scrollbar { display: none; } .gumroad-overlay-iframe { position: absolute; min-width: 100%; min-height: 100%; border: none !important; } @keyframes gumroad-spin { 0% { transform: rotate(0deg); } 100% { transform: rotate(359deg); } } '
    .replace(/GUMROAD_ORIGIN/g, domain);

  const baseStyles = `
    .iframe-container {
        width: 100%;
        height: 100%;
        justify-content: center;
        align-items: center;
        position: fixed;
        background: rgba(0, 0, 0, 0.5);
        top: 0;
        left: 0;
        z-index: 99998;
        display: none;
    }

    .iframe-container.visible {
        display: flex;
    }

    .iframe-container iframe {
        width: 85%;
        height: 85%;
        max-width: 980px;
        max-height: 1080px;
        border: none;
        border-radius: 5px;
    }
  `

  const customTheme = `
  .gumroad-button[data-gumroad-special-button="true"]{
   color: #f19d1e!important;
  }
  
  .gumroad-button[data-gumroad-special-button="true"]::before{
    background: url(GUMROAD_ORIGIN/button/button_logo.png);
    height: 16px;
    display: inline-block;
    content: '';
    width: 16px;
    background-position: center;
    background-size: contain;
    background-repeat: no-repeat;
    margin-bottom: -2px;
    margin-right: 15px;
  }`
  .replace(/GUMROAD_ORIGIN/g, domain);

  return gumroadTheme + baseStyles + customTheme;
}
