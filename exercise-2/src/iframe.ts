export class IFrame {
  private readonly scrollContainer: HTMLDivElement;
  private readonly iFrame: HTMLIFrameElement;
  private loadingPromise?: Promise<void>;

  public constructor() {
    this.getScrollContainer();
    this.iFrame = document.createElement("iframe");
    this.scrollContainer = this.getScrollContainer();
    document.body.append(this.scrollContainer);
    this.scrollContainer.append(this.iFrame);
  }

  private getScrollContainer(): HTMLDivElement {
    const scrollContainer = document.createElement("div");
    scrollContainer.className = "iframe-container";
    scrollContainer.addEventListener('click', () => this.close());
    return scrollContainer
  }

  public async load(src: string): Promise<void> {
    if (!this.loadingPromise) {
      this.iFrame.src = src;
      this.loadingPromise = new Promise((resolve) => {
        this.iFrame.onload = function (e) {
          resolve();
        };
      });
    }
    return this.loadingPromise;
  }

  public show(): void {
    this.scrollContainer.className = 'iframe-container visible';
  }

  public close(): void {
    this.scrollContainer.className = 'iframe-container';
  }

}
