import { IFrame } from './iframe';
import { addStyleSheet } from './styles';
import { GumroadButton } from './gumroad-button';
import { LoadingIndicator } from './loading-indicator';

const TARGET_CLASS = 'gumroad-button';
const DOMAIN = 'https://gumroad.com';
const loadingIndicator = new LoadingIndicator();
const elements = document.getElementsByClassName(TARGET_CLASS);
const iFrameCacheMap = new Map<string, IFrame>();


addStyleSheet(DOMAIN);
addEventListener(elements, 'click', onGumroadButtonClick);
addEventListener(elements, 'mouseover', onGumroadButtonHover);


function addEventListener(elements: HTMLCollectionOf<Element>, eventType: string, callback: (event: Event) => void): void {
  for (let i = 0; i < elements.length; i++) {
    elements[i].addEventListener(eventType, callback, false);
  }
}

async function onGumroadButtonClick(event: Event): Promise<void> {
  event.preventDefault();
  const url = new GumroadButton(event.target as Element).getUrl();
  const iFrame = getCachedIFrame(url);

  loadingIndicator.show();
  await iFrame.load(url);
  loadingIndicator.hide();
  iFrame.show();
}

async function onGumroadButtonHover(event: Event): Promise<void> {
  const gumroadButton = new GumroadButton(event.target as Element);
  if (!gumroadButton.shouldPreload) {
    return;
  }
  const url = gumroadButton.getUrl();
  const iFrame = getCachedIFrame(url);
  console.log('pre-loading');
  await iFrame.load(url);
  console.log('loaded');
}

function getCachedIFrame(url: string): IFrame {
  const cachedIFrame = iFrameCacheMap.get(url);
  if (cachedIFrame != null) {
    return cachedIFrame;
  }
  const newIFrame = new IFrame();
  iFrameCacheMap.set(url, newIFrame);
  return newIFrame;
}
