import { findPivot } from './index';

function getFilledArray(size: number, value: number): number[] {
  return new Array(size).fill(value);
}

const ONE_MILLION = 10000000;

test.each`
  test    | arr    | expected
  ${'Example'} | ${[1, 4, 6, 3, 2]} | ${2}
  ${'Simple'} | ${[1, 6, 1]} | ${1}
  ${'Empty'} | ${[]} | ${-1}
  ${'One element'} | ${[1]} | ${-1}
  ${'Left equals right without element between'} | ${[1, 2, 3]} | ${-1}
  ${'Multiple pivots'} | ${[1, 1, -1, 1, 1]} | ${1}
  ${'Large number of elements on the left'} | ${[...getFilledArray(ONE_MILLION, 1), 10, ONE_MILLION]} | ${ONE_MILLION}
  ${'Large number of elements on the right'} | ${[ONE_MILLION, 10, ...getFilledArray(ONE_MILLION, 1)]} | ${1}
`('$test', ({ arr, expected }) => {
  expect(findPivot(arr)).toBe(expected);
});
