// https://stackoverflow.com/questions/41943700/check-if-sum-of-left-side-of-array-equal-to-right-side-of-array-in-on

export function findPivot(arr: number[]): number {
  const sum = getSum(arr);
  const length = arr.length;

  let sumRight = 0;
  let sumLeft = 0;
  for (let i = 1; i < length - 1; i++) {
    sumLeft += arr[i - 1];
    sumRight = sum - arr[i] - sumLeft;

    if (sumRight === sumLeft) {
      return i;
    }
  }
  return -1;
}

function getSum(arr: number[]): number {
  return arr.reduce((sum, current) => sum + current, 0);
}
